import { Component, OnInit } from '@angular/core';
import { ProductsService } from '../products.service';
import * as jwt_decode from 'jwt-decode';


@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {
  products: any= [];

  constructor(private cartService: ProductsService) { }

  ngOnInit(): void {
    const token = window.localStorage.getItem("token"); 
    const user_id = jwt_decode(token).userid;
    this.cartService.cart(user_id).subscribe((data: any[])=>{
      this.products = data;
      
    });
  }

  // Delete Cart item event
  deleteCart(event) {
    var orderId = event.target.id;
    
    this.cartService.deleteCartItem(orderId).subscribe(data => {
      setTimeout(() => {
        window.location.reload();
      }, 100)
    }, err => {
      window.alert('Error deleting cart item!!');
    })
  }

  // Update cart item event
  update(quantity: any, event) {
    var quantity = quantity;
    var _id = event.target.id;

    

    this.cartService.updateCartItem(_id, quantity).subscribe(data => {
      
      
      window.alert('Cart item not updated!!');
      setTimeout(() => {
        window.location.reload();
      }, 200)
    },
    error => {
      
        setTimeout(() => {
          window.location.reload();
        }, 100)
    })
  }

}
