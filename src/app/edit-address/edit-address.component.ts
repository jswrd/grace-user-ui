import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { UserService } from '../user.service';
import { Router } from '@angular/router';
import * as jwt_decode from 'jwt-decode';

@Component({
  selector: 'app-edit-address',
  templateUrl: './edit-address.component.html',
  styleUrls: ['./edit-address.component.css']
})
export class EditAddressComponent implements OnInit {

  constructor(private changeAdd: UserService, private Router: Router) { }

  ngOnInit(): void {
  }

  // Edit Address event
  adValues(adForm: NgForm): void {
    const street_address = adForm.value.street;
    const city = adForm.value.city;
    const pincode = adForm.value.pincode;
    const state = adForm.value.state;
    const country = adForm.value.country;
    const token = window.localStorage.getItem("token"); 
    const user_id = jwt_decode(token).userid;
    
    
    this.changeAdd.changeAddress(user_id, street_address, city, pincode, state, country).subscribe(data => {
      this.Router.navigate(['/myAccount']);
      window.alert('Address has been changed!!')
    },
    error => {
      window.alert('Error updating address!!')
    }
    )
  }

}
