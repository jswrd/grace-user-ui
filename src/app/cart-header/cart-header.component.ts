import { Component, OnInit } from '@angular/core';
import { ProductsService } from '../products.service';
import * as jwt_decode from 'jwt-decode';

@Component({
  selector: 'app-cart-header',
  templateUrl: './cart-header.component.html',
  styleUrls: ['./cart-header.component.css']
})
export class CartHeaderComponent implements OnInit {

  price: any;

  constructor(private cartService: ProductsService) { }

  ngOnInit(): void {

    const token = window.localStorage.getItem("token"); 
    const user_id = jwt_decode(token).userid;
    this.cartService.totalPrice(user_id).subscribe((data: any[])=> {
      this.price = data;
      
    })
  }

}
