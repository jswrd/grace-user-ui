import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';
import { ProductsService } from '../products.service';
import { UserAuthService } from '../user-auth.service';
import { HttpClient } from '@angular/common/http';
import * as jwt_decode from 'jwt-decode';
import { AppSettings } from '../app-const';

@Component({
  selector: 'app-payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.css']
})
export class PaymentComponent implements OnInit {

  Address:any;
  products:any;
  price:any;
  paytm;
  baseUrl:string = AppSettings.API_ENDPOINT;


  constructor(private user: UserService, private cartService: ProductsService, private userauth: UserAuthService, private http: HttpClient) { }

  // Paytm payment gateway-->>
  ngOnInit(): void {
    const token = window.localStorage.getItem("token"); 
    const user_id = jwt_decode(token).userid;
    this.cartService.paymentDetails(user_id).subscribe((data: any[])=>{
      this.products = data;
      console.log(data["users"][0]["phone_number"]);
      console.log(data["users"][data["users"].length-1]["cost"]);
      var cost = data["users"][data["users"].length-1]["cost"];
      var email = data["users"][0]["email"];
      var order = data["users"][0]["user_id"];
      const phone = data["users"][0]["phone_number"];
      
      this.paytm = {
        MID: "YHtesD71195165286873", // paytm provide
        WEBSITE: "WEBSTAGING", // paytm provide
        INDUSTRY_TYPE_ID: "Retail", // paytm provide
        CHANNEL_ID: "WEB", // paytm provide
        ORDER_ID: `${order}`+ Math.floor(((Math.random() * 5000) * Math.random() * 5000) * Math.random() * 5000 ), // unique id
        CUST_ID: `${order}`, // customer id
        MOBILE_NO: `${phone}`, // customer mobile number
        EMAIL: `${email}`, // customer email
        TXN_AMOUNT: `${cost}`, // transaction amount
        CALLBACK_URL: this.baseUrl + '/orders/confirmation', // Call back URL that i want to redirect after payment fail or success
      };
      

      localStorage.setItem('transactionId', this.paytm.ORDER_ID);
    
      this.payment();
      
      
    });
    
    
    
  
 
  }


  payment() {
    // I will do API call and will get CHECKSUMHASH.
    this.http.post(this.baseUrl + '/orders/payment', this.paytm)
       .subscribe((data: any) => {
             // As per my backend i will get checksumhash under res.data
             
             this.paytm['CHECKSUMHASH'] = data['checksum'];
             
             
             // than i will create form
             this.createPaytmForm();
        })     
};

createPaytmForm() {
  const my_form: any = document.createElement('form');
   my_form.name = 'paytm_form';
   my_form.method = 'post';
   my_form.action = 'https://securegw-stage.paytm.in/order/process';

   const myParams = Object.keys(this.paytm);
   for (let i = 0; i < myParams.length; i++) {
     const key = myParams[i];
     let my_tb: any = document.createElement('input');
     my_tb.type = 'hidden';
     my_tb.name = key;
     my_tb.value = this.paytm[key];
     my_form.appendChild(my_tb);
   };

   document.body.appendChild(my_form);
   my_form.submit();
// after click will fire you will redirect to paytm payment page.
// after complete or fail transaction you will redirect to your CALLBACK URL
};

}
