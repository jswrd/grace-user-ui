import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';
import { Router } from '@angular/router';
import * as jwt_decode from 'jwt-decode';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-edit-login',
  templateUrl: './edit-login.component.html',
  styleUrls: ['./edit-login.component.css']
})
export class EditLoginComponent implements OnInit {

  constructor(private editLogin: UserService, private router: Router) { }

  ngOnInit(): void {
  }

  // Edit login details event
  editValues(editForm: NgForm): void {
    const first_name = editForm.value.fname;
    const last_name = editForm.value.lname;
    const phone_number = editForm.value.phone;
    const email = editForm.value.email;
    const token = window.localStorage.getItem("token"); 
    const user_id = jwt_decode(token).userid;
    
    
    this.editLogin.editLogin(user_id, first_name, last_name, phone_number, email).subscribe(data => {
      
      window.alert('Personal details has been changed!!');
      this.router.navigate(['/myAccount']);
    },
    error => {
      window.alert('Error updating personal details!!')
    }
    )
  }

}
