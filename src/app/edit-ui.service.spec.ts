import { TestBed } from '@angular/core/testing';

import { EditUiService } from './edit-ui.service';

describe('EditUiService', () => {
  let service: EditUiService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EditUiService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
