import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { AppSettings } from './app-const';




@Injectable({
  providedIn: 'root'
})



export class UserAuthService {


  constructor(private http: HttpClient,
    private router: Router,
    ) { }

  baseUrl:string = AppSettings.API_ENDPOINT;

  

  login(_email: any, _password: any) {
    return this.http.post(this.baseUrl + '/users/login', {
      _email, _password
    })
  }

  loggedIn() {
    return !! localStorage.getItem('token')
  }

  logOut() {
    localStorage.removeItem('token')
    setTimeout(() => {
      window.location.reload();
    }, 100);
  }

  getToken() {
    return localStorage.getItem('token')
  }

  public payment() {
    return this.http.post(this.baseUrl + '/orders/payment', {})
  }


}


