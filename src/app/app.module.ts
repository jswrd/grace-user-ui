import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule} from '@angular/common/http'

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule } from '@angular/forms';
import { MaterialModule } from './material/material.module';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HeaderOuterComponent } from './header-outer/header-outer.component';
import { CartHeaderComponent } from './cart-header/cart-header.component';
import { NavbarComponent } from './navbar/navbar.component';
import { SideNavComponent } from './side-nav/side-nav.component';
import { SignupComponent } from './signup/signup.component';
import { HomeComponent } from './home/home.component';
import { AboutComponent } from './about/about.component';
import { HomeCarouselComponent } from './home-carousel/home-carousel.component';
import { TopProductsComponent } from './top-products/top-products.component';
import { FooterComponent } from './footer/footer.component';
import { ServicesComponent } from './services/services.component';
import { ContactUsComponent } from './contact-us/contact-us.component';
import { ContactFormComponent } from './contact-form/contact-form.component';
import { DealersComponent } from './dealers/dealers.component';
import { AllItemsComponent } from './all-items/all-items.component';
import { CartComponent } from './cart/cart.component';
import { CheckoutComponent } from './checkout/checkout.component';
import { ChangeAddressComponent } from './change-address/change-address.component';
import { MyAccountComponent } from './my-account/my-account.component';
import { PaymentComponent } from './payment/payment.component';
import { PaymentConfirmComponent } from './payment-confirm/payment-confirm.component';
import { SearchItemComponent } from './search-item/search-item.component';
import { EditLoginComponent } from './edit-login/edit-login.component';
import { EditAddressComponent } from './edit-address/edit-address.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { TermsComponent } from './terms/terms.component';
import { PrivacyComponent } from './privacy/privacy.component';
import { ShippingComponent } from './shipping/shipping.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderOuterComponent,
    CartHeaderComponent,
    NavbarComponent,
    SideNavComponent,
    SignupComponent,
    HomeComponent,
    AboutComponent,
    HomeCarouselComponent,
    TopProductsComponent,
    FooterComponent,
    ServicesComponent,
    ContactUsComponent,
    ContactFormComponent,
    DealersComponent,
    AllItemsComponent,
    CartComponent,
    CheckoutComponent,
    ChangeAddressComponent,
    MyAccountComponent,
    PaymentComponent,
    PaymentConfirmComponent,
    SearchItemComponent,
    EditLoginComponent,
    EditAddressComponent,
    ResetPasswordComponent,
    TermsComponent,
    PrivacyComponent,
    ShippingComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialModule,
    FormsModule,
    HttpClientModule,
    NgbModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
