import { Component, OnInit } from '@angular/core';
import { ProductsService } from '../products.service';

import * as jwt_decode from 'jwt-decode';

@Component({
  selector: 'app-search-item',
  templateUrl: './search-item.component.html',
  styleUrls: ['./search-item.component.css']
})
export class SearchItemComponent implements OnInit {

  products: any= [];

  constructor(private productService: ProductsService) { }

  ngOnInit(): void {
    const query = window.localStorage.getItem("query");
    // Search item event
    this.productService.search(query).subscribe((data: any[]) => {
      this.products = data;
      
    })
  }

  // Item add to cart event
  addCart(event) {
    const token = window.localStorage.getItem("token"); 
    const user_id = jwt_decode(token).userid;
    var item_id = event.target.id;
    var _status = "Added to cart";


    this.productService.topItems(item_id, user_id, _status, token).subscribe(data => {
      const message = data['message'];
      
      
      if (message==='item has been already added') {
        window.alert('Item has been already added!!')
      } else {
        window.alert('Item added to cart!!!')
        setTimeout(() => {
          window.location.reload();
        }, 100)
      }
      
      
    },
    err => {
      window.alert('Error adding items to cart!!')
    })
  }

}
