import { Component, OnInit } from '@angular/core';
import { UserAuthService } from '../user-auth.service';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-header-outer',
  templateUrl: './header-outer.component.html',
  styleUrls: ['./header-outer.component.css']
})
export class HeaderOuterComponent implements OnInit {
  hide = true;
  token: any = [];
  
  constructor(public logIn: UserAuthService, private Router: Router) { }

  ngOnInit(): void {
  }

  // User login event
  loginValues(loginForm: NgForm): void {
    const _email = loginForm.value.email;
    const _password = loginForm.value.password;
    
    
    this.logIn.login(_email, _password).subscribe(data => {
      this.token = data['token'];
      localStorage.setItem('token', this.token);
      setTimeout(() => {
          window.location.reload();
        }, 100);
      this.Router.navigate(['/myAccount'])
    },
    error => {
      window.alert('Invalid Credentials. Please try again.');
    }
    )
  }

  // Search item event
  searchValues(searchForm: NgForm) {
    let query = searchForm.value.search;
    localStorage.setItem('query', query);
    
  }

  // Reload event
  reload () {
    setTimeout(() => {
      window.location.reload();
    }, 50)
  }

}
