import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AppSettings } from './app-const';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProductsService {

  constructor(private http: HttpClient) { }

  baseUrl:string = AppSettings.API_ENDPOINT;

  public getProducts() {
    return this.http.get(this.baseUrl + '/products/priorityItems');
  }

  public getItems(subProduct: any) {
    return this.http.post(this.baseUrl + '/products/subProductItems', {subProduct});
  }

  public getItemMain(mainProduct: any) {
    return this.http.post(this.baseUrl + '/products/mainProductItems', {mainProduct});
  }

  public topItems(item_id: any, user_id: any, _status: any, token: any) {
    return this.http.post(this.baseUrl + '/orders/addToCart', {item_id, user_id, _status, token});
  }

  public cart(user_id: any) {
    return this.http.post(this.baseUrl + '/orders/showCart', {user_id});
  }

  public deleteCartItem(orderId: number): Observable<any> {
    return this.http.delete(this.baseUrl + `/orders/deleteFromCart/${orderId}`, { responseType: 'text' });
  }

  public updateCartItem(_id: any, quantity: any) {
    return this.http.post(this.baseUrl + '/orders/updateCartItem', {_id, quantity});
  }

  public totalPrice(user_id: any) {
    return this.http.post(this.baseUrl + '/orders/getTotalPrice', {user_id});
  }

  public paymentDetails(user_id: any) {
    return this.http.post(this.baseUrl + '/orders/paymentDetails', {user_id});
  }

  public updateOrders(user_id: any, transId: any) {
    return this.http.post(this.baseUrl + '/orders/confirmOrder', {user_id, transId});
  }

  public search(query: string) {
    return this.http.post(this.baseUrl + '/products/search', {query});
  }

  public placedOrderDetails(user_id: any) {
    return this.http.post(this.baseUrl + '/orders/placedOrderDetails', {user_id});
  }

  public canceledOrderDetails(user_id: any) {
    return this.http.post(this.baseUrl + '/orders/cancelledOrderDetails', {user_id});
  }

  public cancelOrder(_id: any) {
    return this.http.post(this.baseUrl + '/orders/cancelOrder', {_id});
  }

  public refund(_id:any) {
    return this.http.post(this.baseUrl + '/orders/refundRequest', {_id});
  }

  public upadateQuantity(transId: any) {
    return this.http.post(this.baseUrl + '/products/updateQuantity', {transId});
  }
  
}
