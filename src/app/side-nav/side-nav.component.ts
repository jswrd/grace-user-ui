import { Component, OnInit } from '@angular/core';
import { ProductsService } from '../products.service';
import { ItemsService } from '../items.service';

@Component({
  selector: 'app-side-nav',
  templateUrl: './side-nav.component.html',
  styleUrls: ['./side-nav.component.css']
})
export class SideNavComponent implements OnInit {

  message: any;

  constructor(private productService: ProductsService, private data: ItemsService) { }

  ngOnInit(): void {
    this.data.currentMessage.subscribe(message => this.message = message)
  }

  // Get main product name event
  myFunction(evt) {
    const subProduct = evt.target.id;
    this.productService.getItems(subProduct).subscribe((data: any[])=>{
      const items = data;
      this.data.changeMessage(items);
    });
  }

  // Get sub-product name event
  myFun(evt) {
    const mainProduct = evt.target.id;
    this.productService.getItemMain(mainProduct).subscribe((data: any[])=>{
      const items = data;
      this.data.changeMessage(items);
    });
  }

}
