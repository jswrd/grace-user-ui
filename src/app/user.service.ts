import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AppSettings } from './app-const';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) { }

  baseUrl:string = AppSettings.API_ENDPOINT;

  public getAddress(user_id: any) {
    return this.http.post(this.baseUrl + '/users/getAddress', {user_id});
  }

  public enquiry(full_name: any, phone: any, email: any, enquiry:any) {
    return this.http.post(this.baseUrl + '/users/enquiry', {full_name, phone, email, enquiry});
  }

  public changeAddress(user_id: any, street_address: any, city: any, pincode: any, state: any, country: any) {
    return this.http.post(this.baseUrl + '/users/changeAddress', {user_id, street_address, city, pincode, state, country});
  }

  public editLogin(user_id: any, first_name: any, last_name: any, phone_number:any, email:any) {
    return this.http.post(this.baseUrl + '/users/editDetails', {user_id, first_name, last_name, phone_number, email})
  }

  public resetPass(user_id:any, password: any, new_pass:any) {
    return this.http.post(this.baseUrl + '/users/resetPassword', {user_id, password, new_pass});
  }
}
