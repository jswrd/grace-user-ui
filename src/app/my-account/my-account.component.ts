import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';
import { ProductsService } from '../products.service';
import * as jwt_decode from 'jwt-decode';

@Component({
  selector: 'app-my-account',
  templateUrl: './my-account.component.html',
  styleUrls: ['./my-account.component.css']
})
export class MyAccountComponent implements OnInit {

  Address: any;
  Info: any;
  Orders:any;
  CancelledOrders: any;
  div1:boolean=false;
  div2:boolean=false;
  div3:boolean=false;

  constructor(private user: UserService, private cartService: ProductsService) { }

  ngOnInit(): void {
    
  }

  // Get user address event
  getAddress() {
    this.div2=true;
    this.div1=false;
    this.div3=false;
    const token = window.localStorage.getItem("token"); 
    const user_id = jwt_decode(token).userid;
    this.user.getAddress(user_id).subscribe((data: any[])=>{
      this.Address = data;
    });
  }

  // Get user details event
  getInfo() {
    this.div1=true;
    this.div2=false;
    this.div3=false;
    const token = window.localStorage.getItem("token"); 
    const user_id = jwt_decode(token).userid;
    this.user.getAddress(user_id).subscribe((data: any[])=>{
      this.Info = data;
    });
  }

  // Get orders event
  getOrders() {
    this.div3=true;
    this.div2=false;
    this.div1=false;
    const token = window.localStorage.getItem("token"); 
    const user_id = jwt_decode(token).userid;
    // console.log(user_id);
    this.cartService.placedOrderDetails(user_id).subscribe((data: any[])=>{
      this.Orders = data;
      
    });
  }

  // Get cancelled orders event
  getCancelledOrders() {
    this.div3=true;
    this.div2=false;
    this.div1=false;
    const token = window.localStorage.getItem("token"); 
    const user_id = jwt_decode(token).userid;
    this.cartService.canceledOrderDetails(user_id).subscribe((data: any[])=>{
      this.CancelledOrders = data;
      
    });
  }

  // Cancel order event
  cancelOrder(evt) {
    let _id = evt.target.id;
    var result = confirm("ARE YOU SURELY WANT TO CANCEL THIS ORDER?");
    if (result) {
      
      this.cartService.cancelOrder(_id).subscribe((data: any[])=> {
        
        setTimeout(() => {
          window.location.reload();
        }, 100);
    })
    }
    

  }

  // Get refund event
  refund(evt) {
    let _id = evt.target.id;
    
    this.cartService.refund(_id).subscribe(data => {
      const message = data['message'];
      if (message==='refund request sent') {
        window.alert('Refund request has been sent. We will process this as soon as possible!!')
      } else {
        window.alert('Refund request already sent. We are working on refund process!')
      }
    },
    err => {
      window.alert('Error in refunding')
    })
  }

}
