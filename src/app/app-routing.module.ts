import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SignupComponent } from './signup/signup.component';
import { HomeComponent } from './home/home.component';
import { AboutComponent } from './about/about.component';
import { AuthGuard } from './auth.guard';
import { ServicesComponent } from './services/services.component';
import { ContactUsComponent } from './contact-us/contact-us.component';
import { DealersComponent } from './dealers/dealers.component';
import { AllItemsComponent } from './all-items/all-items.component';
import { CartComponent } from './cart/cart.component';
import { CheckoutComponent } from './checkout/checkout.component';
import { ChangeAddressComponent } from './change-address/change-address.component';
import { MyAccountComponent } from './my-account/my-account.component';
import { PaymentComponent } from './payment/payment.component';
import { PaymentConfirmComponent } from './payment-confirm/payment-confirm.component';
import { SearchItemComponent } from './search-item/search-item.component';
import { EditLoginComponent } from './edit-login/edit-login.component';
import { EditAddressComponent } from './edit-address/edit-address.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { TermsComponent } from './terms/terms.component';
import { PrivacyComponent } from './privacy/privacy.component';
import { ShippingComponent } from './shipping/shipping.component';

const routes: Routes = [
  {
    path: 'shipping',
    component: ShippingComponent
  },
  {
    path: 'privacy',
    component: PrivacyComponent
  },
  {
    path: 'terms',
    component: TermsComponent
  },
  {
    path: 'resetPass',
    component: ResetPasswordComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'editAddress',
    component: EditAddressComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'editLogin',
    component: EditLoginComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'searchItem',
    component: SearchItemComponent
  },
  {
    path: 'payment-confirm',
    component: PaymentConfirmComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'payment',
    component: PaymentComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'myAccount',
    component: MyAccountComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'changeAddress',
    component: ChangeAddressComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'checkout',
    component: CheckoutComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'cart',
    component: CartComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'contactUs',
    component: ContactUsComponent
  },
  {
    path: 'allItems',
    component: AllItemsComponent
  },
  {
    path: 'services',
    component: ServicesComponent
  },
  {
    path: 'signup',
    component: SignupComponent
  },
  {
    path: 'dealers',
    component: DealersComponent
  },
  {
    path: 'about',
    component: AboutComponent
  },
  {
    path: '',
    component: HomeComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
