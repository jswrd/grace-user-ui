import { Component, OnInit } from '@angular/core';
import { EditUiService } from '../edit-ui.service';

@Component({
  selector: 'app-privacy',
  templateUrl: './privacy.component.html',
  styleUrls: ['./privacy.component.css']
})
export class PrivacyComponent implements OnInit {

  privacy: any;

  constructor(private edit: EditUiService) { }

  // get dynamic privacy details from server
  ngOnInit(): void {
    this.edit.getPrivacy().subscribe(data => {
      this.privacy = data;
    })
  }

}
