import { Component, OnInit } from '@angular/core';
import { RegisterService } from '../register.service';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {
  hide = true;
  
  constructor(private reg: RegisterService, private Router: Router) { }

  ngOnInit(): void {
  }

  // Submit signup form event
  regValues(regForm: NgForm): void {
    const first_name = regForm.value.fname;
    const last_name = regForm.value.lname;
    const email = regForm.value.email;
    const phone_number = regForm.value.phone;
    const street_address = regForm.value.street;
    const city = regForm.value.city;
    const pincode = regForm.value.pincode;
    const state = regForm.value.state;
    const country = regForm.value.country;
    const _password = regForm.value.password;
    
    
    this.reg.register(first_name, last_name, _password, phone_number, street_address, city, pincode, state, country, email).subscribe(data => {
    
     this.Router.navigate(['/signup']);
     window.alert('Registration Successful!!!')
        setTimeout(() => {
          window.location.reload();
        }, 500)
    },
    error => {
      window.alert('Invalid Credentials. Please try again.')
        setTimeout(() => {
          window.location.reload();
        }, 500)
    }
    )
  }

}
