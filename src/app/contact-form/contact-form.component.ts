import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { UserService } from '../user.service';

@Component({
  selector: 'app-contact-form',
  templateUrl: './contact-form.component.html',
  styleUrls: ['./contact-form.component.css']
})
export class ContactFormComponent implements OnInit {

  constructor(private Router: Router, private user: UserService) { }

  ngOnInit(): void {
  }

  // Submit contact form event
  enValues(enForm: NgForm): void {
    const full_name = enForm.value.fname;
    const email = enForm.value.email;
    const phone = enForm.value.phone;
    const enquiry = enForm.value.enquiry;
    
    
    this.user.enquiry(full_name, phone, email, enquiry).subscribe(data => {
     
     
     this.Router.navigate(['/contactUs']);
     window.alert('Enquiry has been received!!')
        setTimeout(() => {
          window.location.reload();
        }, 200);
    },
    error => {
      window.alert('Invalid Credentials. Please try again.')
    }
    )
  }

}
