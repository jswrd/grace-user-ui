import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http'
import { AppSettings } from './app-const';

@Injectable({
  providedIn: 'root'
})
export class RegisterService {

  constructor(private http: HttpClient) { }

  baseUrl:string = AppSettings.API_ENDPOINT;

  register(first_name: any, last_name: any, _password: any, phone_number: any, street_address: any, city: any, pincode: any, state: any, country: any, email: any){

    return this.http.post(this.baseUrl + '/users/signup', {
      first_name, last_name, _password, phone_number, street_address, city, pincode, state, country, email
    })
  }
}
