import { Component, OnInit } from '@angular/core';
import { EditUiService } from '../edit-ui.service';

@Component({
  selector: 'app-shipping',
  templateUrl: './shipping.component.html',
  styleUrls: ['./shipping.component.css']
})
export class ShippingComponent implements OnInit {

  shipping: any;

  constructor(private edit: EditUiService) { }

  // Get dynamic shipping details from server
  ngOnInit(): void {
    this.edit.getShipping().subscribe(data => {
      this.shipping = data;
    })
  }

}
