import { Component, OnInit } from '@angular/core';
import { ProductsService } from '../products.service';
import { AppSettings } from '../app-const';
import { UserAuthService } from '../user-auth.service';
import * as jwt_decode from 'jwt-decode';


@Component({
  selector: 'app-top-products',
  templateUrl: './top-products.component.html',
  styleUrls: ['./top-products.component.css']
})
export class TopProductsComponent implements OnInit {

  products: any= [];
  

  constructor(private productService: ProductsService,
    private items: UserAuthService) { }

  baseUrl:string = AppSettings.API_ENDPOINT;
  
  // Get all the top products event
  ngOnInit(): void {
    this.productService.getProducts().subscribe((data: any[])=>{
      this.products = data;
    });
  }

  // Item add to cart event
  addCart(event) {
    const token = window.localStorage.getItem("token"); 
    const user_id = jwt_decode(token).userid;
    var item_id = event.target.id;
    var _status = "Added to cart";

    console.log(item_id, user_id, _status);

    this.productService.topItems(item_id, user_id, _status, token).subscribe(data => {
      const message = data['message'];
      
      
      if (message==='item has been already added') {
        window.alert('Item has been already added!!')
      } else {
        window.alert('Item added to cart!!!')
        setTimeout(() => {
          window.location.reload();
        }, 100)
      }
      
      
    },
    err => {
      window.alert('Error adding items to cart!!')
    })
  }
}
