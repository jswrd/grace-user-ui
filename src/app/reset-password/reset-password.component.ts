import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';
import { Router } from '@angular/router';
import * as jwt_decode from 'jwt-decode';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.css']
})
export class ResetPasswordComponent implements OnInit {

  constructor(private editLogin: UserService, private router: Router) { }

  ngOnInit(): void {
  }

  // Update password event
  passValues(passForm: NgForm): void {
    const password = passForm.value.password;
    const new_pass = passForm.value.new_pass;
    const token = window.localStorage.getItem("token"); 
    const user_id = jwt_decode(token).userid;
    
    
    
    this.editLogin.resetPass(user_id, password, new_pass).subscribe(data => {
      
      
      window.alert('Password has been changed!!');
      this.router.navigate(['/myAccount']);
    },
    error => {
      
      window.alert('Error changing password!!')
    }
    )
  }

}
