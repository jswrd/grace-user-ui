import { Component, OnInit } from '@angular/core';
import { ProductsService } from '../products.service';
import { ItemsService } from '../items.service';
import * as jwt_decode from 'jwt-decode';
import { AppSettings } from '../app-const';

@Component({
  selector: 'app-all-items',
  templateUrl: './all-items.component.html',
  styleUrls: ['./all-items.component.css']
})
export class AllItemsComponent implements OnInit {
  baseUrl:string = AppSettings.API_ENDPOINT; // app-const.ts, API endpoint
  products: any= [];
  subProduct: any =[];

  constructor(private productService: ProductsService,private data: ItemsService) { }

  ngOnInit(): void {
    
    // this.productService.getItems().subscribe((data: any[])=>{
    //   this.products = data;
    //   console.log(data);
    // });

    this.data.currentMessage.subscribe(message => this.products = message)
  }
  
  // Add to cart event
  addCart(event) {
    const token = window.localStorage.getItem("token"); 
    const user_id = jwt_decode(token).userid;
    var item_id = event.target.id;
    var _status = "Added to cart";

    this.productService.topItems(item_id, user_id, _status, token).subscribe(data => {
      const message = data['message'];
      
      
      if (message==='item has been already added') {
        window.alert('Item has been already added!!')
      } else {
        window.alert('Item added to cart!!!')
        setTimeout(() => {
          window.location.reload();
        }, 100)
      }
      
      
    },
    err => {
      window.alert('Error adding items to cart!!')
    })
  }

}
