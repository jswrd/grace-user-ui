import { Component, OnInit } from '@angular/core';
import * as jwt_decode from 'jwt-decode';
import { UserService } from '../user.service';
import { ProductsService } from '../products.service';
import { UserAuthService } from '../user-auth.service';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.css']
})
export class CheckoutComponent implements OnInit {

  Address:any;
  products:any;
  price:any;

  constructor(private user: UserService, private cartService: ProductsService, private userauth: UserAuthService, private http: HttpClient) { }

  ngOnInit(): void {
    const token = window.localStorage.getItem("token"); 
    const user_id = jwt_decode(token).userid;
    // get address to show in checkout
    this.user.getAddress(user_id).subscribe((data: any[])=>{
      this.Address = data;
    });

    // get cart item to show in checkout
    this.cartService.cart(user_id).subscribe((data: any[])=>{
      this.products = data; 
    });

    //get total price to show in checkout
    this.cartService.totalPrice(user_id).subscribe((data: any[])=> {
      this.price = data;
      
    });
    
    
    
    
    
    
    
  }

  

  

}
