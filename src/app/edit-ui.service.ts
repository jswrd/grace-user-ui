import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AppSettings } from './app-const';

@Injectable({
  providedIn: 'root'
})
export class EditUiService {

  constructor(private http: HttpClient) { }

  baseUrl:string = AppSettings.API_ENDPOINT;

  getTerms() {
    return this.http.get(this.baseUrl + '/admins/getTerms');
  }

  getPrivacy() {
    return this.http.get(this.baseUrl + '/admins/getPrivacy');
  }

  getShipping() {
    return this.http.get(this.baseUrl + '/admins/getShipping');
  }
}
