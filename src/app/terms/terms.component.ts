import { Component, OnInit } from '@angular/core';
import { EditUiService } from '../edit-ui.service';

@Component({
  selector: 'app-terms',
  templateUrl: './terms.component.html',
  styleUrls: ['./terms.component.css']
})
export class TermsComponent implements OnInit {

  terms: any;

  constructor(private edit: EditUiService) { }

  // Get dynamic terms & conditions from server
  ngOnInit(): void {
    this.edit.getTerms().subscribe(data => {
      this.terms = data;
    })
  }

}
