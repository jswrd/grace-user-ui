import { Component, OnInit } from '@angular/core';
import { ProductsService } from '../products.service';
import * as jwt_decode from 'jwt-decode';

@Component({
  selector: 'app-payment-confirm',
  templateUrl: './payment-confirm.component.html',
  styleUrls: ['./payment-confirm.component.css']
})
export class PaymentConfirmComponent implements OnInit {

  constructor(public update: ProductsService) { }

  // update orders and quantity onInit
  ngOnInit(): void {
    const token = window.localStorage.getItem("token"); 
    const user_id = jwt_decode(token).userid;
    const transId = localStorage.getItem('transactionId');
    
    this.update.updateOrders(user_id, transId).subscribe(data => {
      
      
    });

    this.update.upadateQuantity(transId).subscribe(data => {
      window.location.reload;
    });

  }

}
